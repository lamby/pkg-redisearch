redisearch (1:1.2.2-4) unstable; urgency=medium

  [ Chris Lamb ]
  * Redis modules require executable permissions, otherwise redis-server will
    refuse to start. (Closes: #989385)
  * Drop some binary overrides.
  * Bump debhelper compatibility level to 13.
  * Apply wrap-and-sort -sa.

  [ Debian Janitor ]
  * Apply multi-arch hints. + redis-redisearch-doc: Add Multi-Arch: foreign.

 -- Chris Lamb <lamby@debian.org>  Wed, 02 Jun 2021 16:28:14 +0100

redisearch (1:1.2.2-3) unstable; urgency=medium

  * Sleep to ensure the server has started before running the autopkgtests.

 -- Chris Lamb <lamby@debian.org>  Thu, 23 Apr 2020 13:31:42 +0100

redisearch (1:1.2.2-2) unstable; urgency=medium

  * Ensure that the server is started before running autopkgtests.

 -- Chris Lamb <lamby@debian.org>  Mon, 20 Apr 2020 23:46:17 +0100

redisearch (1:1.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0.
  * Add Rules-Requires-Root: no.

 -- Chris Lamb <lamby@debian.org>  Wed, 15 Apr 2020 11:34:54 +0100

redisearch (1:1.2.1-4) unstable; urgency=medium

  * Pass -ffile-prefix-map for a reproducible build.

 -- Chris Lamb <lamby@debian.org>  Tue, 22 Jan 2019 10:22:39 +0000

redisearch (1:1.2.1-3) unstable; urgency=medium

  * Check for __FreeBSD_kernel__ over __FreeBSD__ for CLOCK_MONOTONIC_RAW.
    Thanks to James Clarke for the pointer.

 -- Chris Lamb <lamby@debian.org>  Mon, 14 Jan 2019 00:05:08 +0000

redisearch (1:1.2.1-2) unstable; urgency=medium

  * Define CLOCK_MONOTONIC_RAW for kFreeBSD.
  * debian/control:
    - Bump debhelper compatibility level.
    - Bump Standards-Version to 4.3.0.

 -- Chris Lamb <lamby@debian.org>  Wed, 02 Jan 2019 17:18:52 +0000

redisearch (1:1.2.1-1) unstable; urgency=medium

  * Package the last AGPLv3 (ie. non Commons Clause) package from the GoodFORM
    project <https://goodformcode.com/>, regrettably bumping the epoch.
    - Update debian/copyright.
    - Update debian/watch.
    - Update Homepage field.
  * Use debhelper-compat virtual package and drop debian/compat.
  * Bump Standards-Version to 4.2.1.

 -- Chris Lamb <lamby@debian.org>  Wed, 24 Oct 2018 20:48:33 -0400

redisearch (1.4.0~rc3-1) unstable; urgency=medium

  * New upstream RC release.
  * debian/watch: Also mangle "rc" releases.

 -- Chris Lamb <lamby@debian.org>  Sun, 19 Aug 2018 09:48:57 +0100

redisearch (1.4.0~rc1-1) unstable; urgency=medium

  * New upstream RC release.

 -- Chris Lamb <lamby@debian.org>  Thu, 16 Aug 2018 09:53:59 +0100

redisearch (1.3.0~preview3-1) unstable; urgency=medium

  * New upstream preview release.
  * Bump Standards-Version to 4.2.0.

 -- Chris Lamb <lamby@debian.org>  Sat, 11 Aug 2018 18:22:01 +0200

redisearch (1.3.0~preview2-1) unstable; urgency=medium

  * New upstream preview release.
    - Add cmake to Build-Depends.
  * debian/watch: Support beta/preview suffixes.
  * Bump Standards-Version to 4.1.5.

 -- Chris Lamb <lamby@debian.org>  Thu, 02 Aug 2018 10:12:40 +0800

redisearch (1.2.0-2) unstable; urgency=medium

  * Update Vcs-* to point to salsa.debian.org.

 -- Chris Lamb <lamby@debian.org>  Fri, 08 Jun 2018 17:47:16 +0100

redisearch (1.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Tue, 15 May 2018 08:42:32 +0100

redisearch (1.1.0-1) unstable; urgency=medium

  * New upstream release.
    - Drop debian/patches/0001-remove-mpopcnt-and-opt-for-simple-pop-counting;
      applied upstream.

 -- Chris Lamb <lamby@debian.org>  Wed, 25 Apr 2018 07:53:20 +0100

redisearch (1.0.10-3) unstable; urgency=medium

  * Use upstream's patch for removing -mpopcnt.

 -- Chris Lamb <lamby@debian.org>  Tue, 24 Apr 2018 22:35:11 +0100

redisearch (1.0.10-2) unstable; urgency=medium

  * Drop -mpopcnt from CLAGS. This was added upstream in
    <https://github.com/RedisLabsModules/RediSearch/commit/a693e0e1c92e629bfdc78afcd00b1050e68dd238>.
    (Closes: #896593)

 -- Chris Lamb <lamby@debian.org>  Sun, 22 Apr 2018 22:16:23 +0200

redisearch (1.0.10-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.4.

 -- Chris Lamb <lamby@debian.org>  Sat, 21 Apr 2018 21:52:16 +0200

redisearch (1.0.9-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Fri, 23 Mar 2018 18:20:23 -0400

redisearch (1.0.8-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 28 Feb 2018 13:59:26 +0000

redisearch (1.0.7-1) unstable; urgency=medium

  * New upstream realease.
  * Move debhelper Build-Depends from 11 to 11~.
  * Override package-does-not-install-examples; these are for a dependency.

 -- Chris Lamb <lamby@debian.org>  Mon, 12 Feb 2018 10:10:43 +0000

redisearch (1.0.6-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Fri, 02 Feb 2018 09:29:42 +0000

redisearch (1.0.5-1) unstable; urgency=medium

  * New upstream release.
  * Use HTTPS URI for debian/copyright "Format" URI.

 -- Chris Lamb <lamby@debian.org>  Wed, 24 Jan 2018 22:30:20 +1100

redisearch (1.0.4-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Fri, 12 Jan 2018 10:26:07 +0530

redisearch (1.0.3-1) unstable; urgency=medium

  * New upstream release.
  * Remove 0001-Use-build-flags-from-outside-environment.patch; applied
    upstream.
  * Bump debhelper compat level to 11.
  * Bump Standards-Version to 4.1.3.
  * Don't install README.md into both binary packages.

 -- Chris Lamb <lamby@debian.org>  Sat, 06 Jan 2018 11:11:34 +0000

redisearch (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Update 0001-Use-build-flags-from-outside-environment.patch to ensure .so
    file is hardened. Filed upstream:
    <https://github.com/RedisLabsModules/RediSearch/pull/245>
  * Update upstream's .gitignore so our changes to debian/ are visible without
    -f. Filed upstream as
    <https://github.com/RedisLabsModules/RediSearch/pull/244>.
  * Override no-upstream-changelog in all binary packages.

 -- Chris Lamb <lamby@debian.org>  Fri, 15 Dec 2017 17:49:39 +0000

redisearch (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.2.

 -- Chris Lamb <lamby@debian.org>  Sun, 10 Dec 2017 20:52:30 +0000

redisearch (1.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 29 Nov 2017 08:07:16 +0900

redisearch (0.99.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Mon, 27 Nov 2017 20:18:03 +0900

redisearch (0.99.0-1) unstable; urgency=medium

  * New upstream release.
  * Add override as upstream are not providing GPG signatures.
  * debian/watch:
    - Move to version 4 format.
    - Strip leading v from upstream URL.

 -- Chris Lamb <lamby@debian.org>  Sat, 18 Nov 2017 16:56:45 +0900

redisearch (0.90.1-1) unstable; urgency=medium

  * New upstream release.
    - Fix FTBFS on 32-bits architectures. (Closes: #880801)

 -- Chris Lamb <lamby@debian.org>  Sun, 05 Nov 2017 15:37:39 +0000

redisearch (0.90.0~alpha1-1) unstable; urgency=medium

  * New upstream alpha release.
  * debian/patches:
    - Refresh 0001-Use-build-flags-from-outside-environment.patch.
    - Drop 0002-Don-t-assume-that-non-Linux-is-OSX.-Closes-879948.patch;
      applied upstream.
  * debian/watch: Match all releases.

 -- Chris Lamb <lamby@debian.org>  Thu, 02 Nov 2017 19:51:29 +0100

redisearch (0.21.3-5) unstable; urgency=medium

  * Don't assume that non-Linux is OSX. (Closes: #879948)

 -- Chris Lamb <lamby@debian.org>  Fri, 27 Oct 2017 16:09:14 +0100

redisearch (0.21.3-4) unstable; urgency=medium

  * Install to /usr/lib/redis/modules/redisearch.so, not
    /var/lib/redis/modules/redisearch.so.

 -- Chris Lamb <lamby@debian.org>  Thu, 26 Oct 2017 12:53:15 +0100

redisearch (0.21.3-3) unstable; urgency=medium

  * Update debian/copyright.

 -- Chris Lamb <lamby@debian.org>  Wed, 25 Oct 2017 10:05:45 -0400

redisearch (0.21.3-2) unstable; urgency=medium

  * Specify Section: doc for redis-redisearch-doc.
  * Enable bindnow hardening.
  * Use build flags from outside environment.

 -- Chris Lamb <lamby@debian.org>  Tue, 24 Oct 2017 19:08:34 -0400

redisearch (0.21.3-1) unstable; urgency=medium

  * Initial release. (Closes: #879203)

 -- Chris Lamb <lamby@debian.org>  Tue, 24 Oct 2017 18:29:18 -0400
